#! /usr/bin/env python
try:
      import setuptools
      from setuptools import setup
except ImportError:
    print('setuptools is not installed')

setup(name='eecmip5',
      description='Download CMIP5 climate simulations from Google Earth Engine',
      version='0.0.7',
      license="MIT",
      classifiers = [
         "Topic :: Scientific/Engineering :: Atmospheric Science",
         "Development Status :: 2 - Pre-Alpha",
         "Intended Audience :: Developers",
         "License :: OSI Approved :: MIT License",
         "Programming Language :: Python :: 3.5"
      ],
      url='https://gitlab.com/csaybar/eecmip5',
      download_url = 'https://gitlab.com/csaybar/eecmip5/-/archive/master/eecmip5-master.tar.gz',
      keywords = ['climate', 'data', 'CMIP5','download', 'processing'],
      author='Cesar Aybar Camacho',
      author_email='csaybar@gmail.com',
      packages=setuptools.find_packages(),
      )