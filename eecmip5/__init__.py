__version__ = '0.0.7'

import sys

major, minor = sys.version_info.major, sys.version_info.minor

if major < 3:
    sys.exit("Sorry, Python 2 is not supported. You need Python >= 3.5 for eecmip5")
elif minor < 5:
    sys.exit("Sorry, You need Python >= 3.5 for eecmip5")

#from .cmip5 import *